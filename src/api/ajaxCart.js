export default class AjaxCart {
	constructor(client) {
		this.client = client;
	}

	list() {
		return this.client.get(`/orders`);
	}

	byId(id) {
		return this.client.get(`/orders/${id}`);
	}

	retrieve(cookie) {
		return this.client.get(`/cart`, null, cookie);
	}

	update(data) {
		return this.client.put(`/cart`, data);
	}

	checkout(data) {
		return this.client.put(`/cart/checkout`, data);
	}
	charge(data) {
		return this.client.post(`/cart/charge`, data);
	}

	checkoutInstant(data) {
		return this.client.post(`/cart/checkoutInstant`, data);
	}

	updateBillingAddress(address) {
		return this.client.put(`/cart/billing_address`, address);
	}

	updateShippingAddress(address) {
		return this.client.put(`/cart/shipping_address`, address);
	}

	addItem(data) {
		return this.client.post(`/cart/items`, data);
	}

	updateItem(id, data) {
		return this.client.put(`/cart/items/${id}`, data);
	}

	deleteItem(id) {
		return this.client.delete(`/cart/items/${id}`);
	}
}
