export default class Checkin {
	constructor(client) {
		this.client = client;
		this.resourceUrl = '/checkin';
	}

	list(filter) {
		return this.client.get(this.resourceUrl, filter);
	}

	create(data) {
		return this.client.post(this.resourceUrl, data);
	}

}
