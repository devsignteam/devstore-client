export default class AjaxPaymentMethods {
	constructor(client) {
		this.client = client;
	}

	list() {
		return this.client.get('/payment_methods');
	}
	list1() {
		return this.client.get('/payment_form_settings');
	}
}
