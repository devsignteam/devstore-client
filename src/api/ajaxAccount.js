export default class AjaxAccount {
	constructor(client) {
		this.client = client;
	}

	retrieve(data) {
		return this.client.post(`/customer-account`, data);
	}
	
	attachPaymentMethod(data) {
		return this.client.post(`/attach_payment_method`, data);
	}

	update(data) {
		return this.client.put(`/customer-account`, data);
	}
}
