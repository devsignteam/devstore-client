export default class Products {
	constructor(client) {
		this.client = client;
		this.resourceUrl = '/products';
	}

	uploadExcelFile(formData) {
		return this.client.postFormData(
			`${this.resourceUrl}/excel-template`,
			formData
		);
	}
}
