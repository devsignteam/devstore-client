import AjaxClient from './ajaxClient';
import ApiClient from './apiClient';
import WebStoreClient from './webstoreClient';
import ProductCategories from './api/productCategories';
import Products from './api/products/products';
import UploadProductExcel from './api/fileUpload/products';
import ProductOptions from './api/products/options';
import ProductOptionValues from './api/products/optionValues';
import ProductVariants from './api/products/variants';
import ProductImages from './api/products/images';
import Sitemap from './api/sitemap';
import Theme from './api/theme/theme';
import ThemeSettings from './api/theme/settings';
import ThemeAssets from './api/theme/assets';
import ThemePlaceholders from './api/theme/placeholders';
import CustomerGroups from './api/customerGroups';
import Customers from './api/customers';
import AjaxCheckin from './api/checkin';
import AjaxCart from './api/ajaxCart';
import AjaxLogin from './api/ajaxLogin';
import AjaxRegister from './api/ajaxRegister';
import AjaxAccount from './api/ajaxAccount';
import AjaxForgotPassword from './api/ajaxForgotPassword';
import AjaxResetPassword from './api/ajaxResetPassword';
import Orders from './api/orders/orders';
import OrderDiscounts from './api/orders/discounts';
import OrderTransactions from './api/orders/transactions';
import OrderItems from './api/orders/items';
import OrderStatuses from './api/orders/statuses';
import ShippingMethods from './api/shippingMethods';
import PaymentMethods from './api/paymentMethods';
import PaymentGateways from './api/paymentGateways';
import AjaxShippingMethods from './api/ajaxShippingMethods';
import AjaxPaymentMethods from './api/ajaxPaymentMethods';
import AjaxPaymentFormSettings from './api/ajaxPaymentFormSettings';
import Countries from './api/countries';
import Currencies from './api/currencies';
import Text from './api/text';
import Settings from './api/settings';
import CheckoutFields from './api/checkoutFields';
import Pages from './api/pages';
import Tokens from './api/tokens';
import Redirects from './api/redirects';
import Webhooks from './api/webhooks';
import Files from './api/files';
import AppSettings from './api/apps/settings';
import WebStoreAccount from './webstore/account';
import WebStoreServices from './webstore/services';
import WebStoreServiceSettings from './webstore/serviceSettings';
import WebStoreServiceActions from './webstore/serviceActions';
import WebStoreServiceLogs from './webstore/serviceLogs';
import UserApiClient from './userApiClient';

export default class Client {
	constructor(options = {}) {
		this.reinit(options);
	}

	reinit(options = {}) {
		this.apiBaseUrl = options.apiBaseUrl || this.apiBaseUrl || '/api/v1';
		this.apiToken = options.apiToken || this.apiToken;
		this.userToken = options.userToken || this.userToken;
		this.ajaxBaseUrl = options.ajaxBaseUrl || this.ajaxBaseUrl || '/ajax';
		this.userApiBaseUrl = options.userApiBaseUrl || this.userApiBaseUrl || '/userApi';
		this.webstoreToken = options.webstoreToken || this.webstoreToken;

		const apiClient = new ApiClient({
			baseUrl: this.apiBaseUrl,
			token: this.apiToken
		});
		const ajaxClient = new AjaxClient({
			baseUrl: this.ajaxBaseUrl,
			token: this.userToken
		});
		const userApiClient = new UserApiClient({
			baseUrl: this.userApiBaseUrl,
			token: this.userToken
		});
		const webstoreClient = new WebStoreClient({ token: this.webstoreToken });

		this.products = new Products(apiClient);
		this.upload = new UploadProductExcel(apiClient);
		this.products.options = new ProductOptions(apiClient);
		this.products.options.values = new ProductOptionValues(apiClient);
		this.products.variants = new ProductVariants(apiClient);
		this.products.images = new ProductImages(apiClient);
		this.productCategories = new ProductCategories(apiClient);
		this.customers = new Customers(apiClient);
		this.orders = new Orders(apiClient);
		this.orders.discounts = new OrderDiscounts(apiClient);
		this.orders.transactions = new OrderTransactions(apiClient);
		this.orders.items = new OrderItems(apiClient);
		this.orderStatuses = new OrderStatuses(apiClient);
		this.shippingMethods = new ShippingMethods(apiClient);
		this.paymentMethods = new PaymentMethods(apiClient);
		this.paymentGateways = new PaymentGateways(apiClient);
		this.customerGroups = new CustomerGroups(apiClient);
		this.sitemap = new Sitemap(apiClient);
		this.theme = new Theme(apiClient);
		this.theme.settings = new ThemeSettings(apiClient);
		this.theme.assets = new ThemeAssets(apiClient);
		this.theme.placeholders = new ThemePlaceholders(apiClient);
		this.countries = new Countries(apiClient);
		this.currencies = new Currencies(apiClient);
		this.text = new Text(apiClient);
		this.settings = new Settings(apiClient);
		this.checkoutFields = new CheckoutFields(apiClient);
		this.pages = new Pages(apiClient);
		this.tokens = new Tokens(apiClient);
		this.redirects = new Redirects(apiClient);
		this.webhooks = new Webhooks(apiClient);
		this.files = new Files(apiClient);
		this.apps = {};
		this.apps.settings = new AppSettings(apiClient);

		this.ajax = {};
		this.ajax.products = new Products(ajaxClient);
		this.ajax.categories = new ProductCategories(ajaxClient);
		this.ajax.sitemap = new Sitemap(ajaxClient);
		this.ajax.cart = new AjaxCart(ajaxClient);
		this.ajax.login = new AjaxLogin(ajaxClient);
		this.ajax.register = new AjaxRegister(ajaxClient);
		this.ajax.account = new AjaxAccount(ajaxClient);
		this.ajax.checkin = new AjaxCheckin(ajaxClient);
		this.ajax.forgotPassword = new AjaxForgotPassword(ajaxClient);
		this.ajax.resetPassword = new AjaxResetPassword(ajaxClient);
		this.ajax.countries = new Countries(ajaxClient);
		this.ajax.currencies = new Currencies(ajaxClient);
		this.ajax.shippingMethods = new AjaxShippingMethods(ajaxClient);
		this.ajax.paymentMethods = new AjaxPaymentMethods(ajaxClient);
		this.ajax.paymentGateways = new PaymentGateways(ajaxClient);
		this.ajax.paymentFormSettings = new AjaxPaymentFormSettings(ajaxClient);
		this.ajax.pages = new Pages(ajaxClient);

		this.userApi = {};
		this.userApi.products = new Products(userApiClient);
		this.userApi.categories = new ProductCategories(userApiClient);
		this.userApi.sitemap = new Sitemap(userApiClient);
		this.userApi.cart = new AjaxCart(userApiClient);
		this.userApi.login = new AjaxLogin(userApiClient);
		this.userApi.register = new AjaxRegister(userApiClient);
		this.userApi.account = new AjaxAccount(userApiClient);
		this.userApi.forgotPassword = new AjaxForgotPassword(userApiClient);
		this.userApi.resetPassword = new AjaxResetPassword(userApiClient);
		this.userApi.countries = new Countries(userApiClient);
		this.userApi.currencies = new Currencies(userApiClient);
		this.userApi.shippingMethods = new AjaxShippingMethods(userApiClient);
		this.userApi.paymentMethods = new AjaxPaymentMethods(userApiClient);
		this.userApi.paymentGateways = new PaymentGateways(userApiClient);
		this.userApi.paymentFormSettings = new AjaxPaymentFormSettings(
			userApiClient
		);
		this.userApi.pages = new Pages(userApiClient);

		this.webstore = {};
		this.webstore.account = new WebStoreAccount(webstoreClient);
		this.webstore.services = new WebStoreServices(webstoreClient);
		this.webstore.services.settings = new WebStoreServiceSettings(
			webstoreClient
		);
		this.webstore.services.actions = new WebStoreServiceActions(webstoreClient);
		this.webstore.services.logs = new WebStoreServiceLogs(webstoreClient);
	}

	static authorize = (baseUrl, email) => ApiClient.authorize(baseUrl, email);

	static authorizeEmailPassword = (baseUrl, email, password) =>
		ApiClient.authorizeEmailPassword(baseUrl, email, password);

	static authorizeInWebStore = (email, adminUrl) =>
		WebStoreClient.authorize(email, adminUrl);
}
