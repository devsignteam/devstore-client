import RestClient from './restClient';

export default class AjaxClient extends RestClient {
	getConfig(method, data, cookie) {
		const config = {
			method,
			headers: {
				'Content-Type': 'application/json',
				Authorization: `${this.token}`
			}
		};

		if (cookie) {
			config.headers.Cookie = cookie;
		}

		if (data) {
			config.body = JSON.stringify(data);
		}
		return config;
	}

	getCredentialsConfig(baseUrl) {
		const includePrefix =
			baseUrl.includes('http://') || baseUrl.includes('https://');
		return includePrefix ? 'include' : 'same-origin';
	}
}
