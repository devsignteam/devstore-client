import RestClient from './restClient';

export default class UserApiClient extends RestClient {
	getConfig(method, data, cookie) {
		const config = {
			method,
			headers: {
				'Content-Type': 'application/json',
				Authorization: `${this.token}`
			}
		};

		if (cookie) {
			config.headers.Cookie = cookie;
		}

		if (data) {
			config.body = JSON.stringify(data);
		}
		return config;
	}
}
