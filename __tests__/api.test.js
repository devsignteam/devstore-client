const DevstoreClient = require('../lib/index').default;

const options = {
	ajaxBaseUrl: 'http://localhost:3001/ajax',
	userApiBaseUrl: 'http://localhost:3001/userApi',
	userToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZThiNjg5NWZlZTVlNTI1N2JkMTZiNzEiLCJpYXQiOjE1ODY2MjM1NTl9.1YIybL-t1L9YYYP_O_pWi_jKdM5dPG3WYjOIJzkbx04'
};

const api = new DevstoreClient(options);

// api.userApi.login
// 	.retrieve({
// 		email: 'iamharrytran@gmail.com',
// 		password: '123456'
// 	})
// 	.then(result => {
// 		console.log(result);
// 	})
// 	.catch(error => {
// 		console.log(error);
// 	});

// api.userApi.register
// 	.registerAccount({
// 		email: 'iamharrytran@gmail.com',
// 		password: '123456'
// 	})
// 	.then(result => {
// 		console.log(result);
// 	})
// 	.catch(error => {
// 		console.log(error);
// 	});

api.ajax.products
	.list({
		category_slug: 'beverages'
	})
	.then(result => {
		console.log(result);
	})
	.catch(error => {
		console.log(error);
	});

// api.userApi.cart.deleteItem('5e91e301de3fa27a8f77fc3f').then(result => {
// 	console.log(result.json)
// })

// api.userApi.cart.retrieve().then(result => {
// 	console.log(result.json)
// })