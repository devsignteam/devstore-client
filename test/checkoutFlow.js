const DevStoreClient = require("../lib");

const client = new DevStoreClient.default({
    // apiBaseUrl: "https://mb-devstore-backend-dev.ap.ngrok.io/api/v1",
    // ajaxBaseUrl: "https://mb-devstore-backend-dev.ap.ngrok.io/ajax",
    // userApiBaseUrl: "https://mb-devstore-backend-dev.ap.ngrok.io/userApi",
    apiBaseUrl: "http://localhost:4001/api/v1",
    ajaxBaseUrl: "http://localhost:4001/ajax",
    userApiBaseUrl: "http://localhost:4001/userApi",
    userToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZmNkZGUzNDJmMTg1MTBlZDBiMjVlZDUiLCJpYXQiOjE2MDc0MDk1MjJ9.Nxg6VC3dgWLBACXyMmzj_KLqkoUYCzXUDTGmCQqArO0',
});

// const client = new DevStoreClient.default({
//     apiBaseUrl: "https://mb-devstore-backend-dev.ap.ngrok.io/api/v1",
//     ajaxBaseUrl: "https://mb-devstore-backend-dev.ap.ngrok.io/ajax",
//     userApiBaseUrl: "https://mb-devstore-backend-dev.ap.ngrok.io/userApi",
//     userToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZmM5ZmE2NmQ4MzY2MjU3OWZjZDhiY2YiLCJpYXQiOjE2MDcwNzI5MDN9.u_JHCF1Azz1ztrhDREFnVdKKt2WM08N0Jp3M7BWWdEg',
// });

// client.ajax.register.retrieve({
//     email: "iamharrytran@gmail.com",
//     password: "123456",
//     firstName: "Luu",
//     lastName: "Hieu"
// }).then(obj => {
//     console.log(obj)
// });

// client.ajax.login.retrieve({
//     email: "neoreul.qn96@gmail.com",
//     password: "huyuyhuhy",
// }).then(obj => {
//     console.log(obj)
// });

// client.userApi.cart.addItem({
//     product_id: "5fca21fc9bf2b6fedec0608d",
//     quantity: 1,
//     slug: 'repair-gel'
// }).then(obj => {
//     client.ajax.cart.checkout({
//         orderId: obj.json._id,
//     }).then(obj => {
//         client.ajax.cart.charge({
//             orderId: obj.json._id,
//             paymentMethodId: '5e0c27c3b6099c61571df6d6'
//         }).then(obj => {
//             console.log(obj)
//             client.userApi.cart.retrieve().then(obj => {
//                 console.log(obj)
//             });
//         });
//     });
// });

// client.ajax.products.findBySlug('repair-gel').then(obj => {
//     console.log(obj)
// });

// client.ajax.paymentMethods.list().then(obj => {
//     console.log(obj)
// });

// client.userApi.cart.list().then(obj => {
//     console.log(obj.json.data)
// });


// client.ajax.account.attachPaymentMethod({
//     paymentMethodId: "tok_1Hua3rJXDrwPrAznEy52l9EI"
// }).then(obj => {
//     console.log(obj)
// });



// client.ajax.checkin.create({
//     // userId: '5e0c27c3b6099c61571df6d6',
//     search: 'vandoan.qn96@gmail1.com',
// }).then(obj => {
//     console.log(obj)
// });

