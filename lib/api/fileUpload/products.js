'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Products = function () {
	function Products(client) {
		_classCallCheck(this, Products);

		this.client = client;
		this.resourceUrl = '/products';
	}

	_createClass(Products, [{
		key: 'uploadExcelFile',
		value: function uploadExcelFile(formData) {
			return this.client.postFormData(this.resourceUrl + '/excel-template', formData);
		}
	}]);

	return Products;
}();

exports.default = Products;